<?php

namespace Maesbox\MusiqueBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class MusiqueAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title')
            ->add('band', 'sonata_type_model')
            ->add('artist', 'sonata_type_model')
            ->add('album', 'sonata_type_model')    
            ->add('genre', 'sonata_type_model')
            ->add('track_number')
            ->add('file')
            ->add('fileSize')
            ->add('fileSystem', 'sonata_type_model')
            ->add('year')
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('band')
            ->add('artist')
            ->add('album')
            ->add('genre')
            ->add('track_number')
            ->add('year')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->add('duration')
            ->add('year')
            ->add('band')
            ->add('artist')
            ->add('album')
            ->add('genre')
            ->add('track_number')
            ->add('created_at')
            ->add('updated_at')
        ;
    }
}