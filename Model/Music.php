<?php

namespace Maesbox\MusiqueBundle\Model;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

abstract class Musique
{
    
    /**
     *
     * @var band
     * @ORM\ManyToOne(targetEntity="Band", inversedBy="musiques", cascade={"persist"})
     * @ORM\JoinColumn(name="band_id", referencedColumnName="id", nullable=true)
     */
    protected $band;
    
    /**
     *
     * @var artist
     * @ORM\ManyToOne(targetEntity="Artist", inversedBy="musiques", cascade={"persist"})
     * @ORM\JoinColumn(name="artist_id", referencedColumnName="id", nullable=true)
     */
    protected $artist;
    
    /**
     *
     * @var album
     * @ORM\ManyToOne(targetEntity="Album", inversedBy="musiques", cascade={"persist"})
     * @ORM\JoinColumn(name="album_id", referencedColumnName="id", nullable=true)
     */
    protected $album;
    
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", nullable=true)
     */
    protected $title;
    
    /**
     * @var float
     * @ORM\Column(name="duration", type="float", nullable=true)
     */
    protected $duration;
    
    /**
     * @var genre
     * @ORM\ManyToOne(targetEntity="Genre", cascade={"persist"})
     * @ORM\JoinColumn(name="genre_id", referencedColumnName="id", nullable=true)
     */
    protected $genre;
    
    protected $composer;
    
    /**
     * @var integer
     * @ORM\Column(name="track_number", type="integer", nullable=true)
     */
    protected $track_number;
    
    /**
     * @var integer
     * @ORM\Column(name="year", type="integer", nullable=true)
     */
    protected $year;
    
    
    public function __toString() {
        return $this->title;
    }
}

?>
