MaesboxMusiqueBundle
====================
[![Latest Stable Version](https://poser.pugx.org/maesbox/musiquebundle/v/stable.svg)](https://packagist.org/packages/maesbox/musiquebundle) [![Total Downloads](https://poser.pugx.org/maesbox/musiquebundle/downloads.svg)](https://packagist.org/packages/maesbox/musiquebundle) [![Latest Unstable Version](https://poser.pugx.org/maesbox/musiquebundle/v/unstable.svg)](https://packagist.org/packages/maesbox/musiquebundle) [![License](https://poser.pugx.org/maesbox/musiquebundle/license.svg)](https://packagist.org/packages/maesbox/musiquebundle)

Présentation 
--------------------

Ce bundle permet la gestion des fichiers musicaux sous forme de médiathèque.
Il permet d'organiser les fichiers audios par artiste, albums, etc...

Installation
--------------------

- installation avec composer

*dans le composer.json*
~~~~

    "repositories": [
        {
            "type": "package",
            "package": {
                "name": "jquery/jquery",
                "version": "1.11.1",
                "dist": {
                    "url": "http://code.jquery.com/jquery-1.11.1.js",
                    "type": "file"
                }
            }
        }
    ],
    "require": {
        ...
        "maesbox/musiquebundle": "dev-master",
        "jquery/jquery":  "1.11.*"
        ...
    },

~~~~


Configuration
--------------------
