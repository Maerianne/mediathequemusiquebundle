<?php

namespace Maesbox\MusiqueBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('MaesboxMusiqueBundle:Default:index.html.twig', array('name' => $name));
    }
}
