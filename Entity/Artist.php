<?php
namespace Maesbox\MusiqueBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Maesbox\MusiqueBundle\Model\ArtistInterface;

/**
 * Musique
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Maesbox\MusiqueBundle\Entity\ArtistRepository")
 */
class Artist
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Maesbox\MusiqueBundle\Model\ArtistInterface")
     * @var ArtistInterface
     
    protected $person;*/
    
    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     */
    protected $name;
    
    /**
     * @ORM\Column(length=5000, nullable=true)
     * @Gedmo\Translatable
     */
    protected $biography;
    
    /*
     * @ORM\OneToMany(targetEntity="Musique", mappedBy="artist")
     */
    protected $musiques;
    
    /*
     * @ORM\OneToMany(targetEntity="Album", mappedBy="artist")
     */
    protected $albums;
    
    /**
     * @var date
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $created_at;
    
    /**
     * @var date
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updated_at;
    
    public function __toString() {
        return $this->name;
    }
    
    /**
    * @ORM\PrePersist
    */
   public function setCreatedValue()
   {
       $this->created_at = new \DateTime();
   }

   /**
    * @ORM\PreUpdate
    */
   public function setUpdatedValue()
   {
       $this->updated_at = new \DateTime();
   }
    
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Artist
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set biography
     *
     * @param string $biography
     * @return Artist
     */
    public function setBiography($biography)
    {
        $this->biography = $biography;
    
        return $this;
    }

    /**
     * Get biography
     *
     * @return string 
     */
    public function getBiography()
    {
        return $this->biography;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Artist
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Artist
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    
        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
}