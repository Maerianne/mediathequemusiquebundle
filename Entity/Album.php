<?php

namespace Maesbox\MusiqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Maesbox\MusiqueBundle\Entity\Musique;

/**
 * Video
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Maesbox\MusiqueBundle\Entity\AlbumRepository")
 */
class Album 
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     * @ORM\Column(name="title", type="string")
     */
    protected $title;

    /**
     * @var integer
     * @ORM\Column(name="year", type="integer", nullable=true)
     */
    protected $year;
    
    /**
     * @var genre
     * @ORM\ManyToOne(targetEntity="Genre", cascade={"persist"})
     * @ORM\JoinColumn(name="genre_id", referencedColumnName="id", nullable=true)
     */
    protected $genre;
    
    /**
     *
     * @var band
     * @ORM\ManyToOne(targetEntity="Band", inversedBy="albums", cascade={"persist"})
     * @ORM\JoinColumn(name="band_id", referencedColumnName="id", nullable=true)
     */
    protected $band;
    
    /**
     *
     * @var artist
     * @ORM\ManyToOne(targetEntity="Artist", inversedBy="albums", cascade={"persist"})
     * @ORM\JoinColumn(name="artist_id", referencedColumnName="id", nullable=true)
     */
    protected $artist;
        
    /**
     * @var date
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $created_at;
    
    /**
     * @var date
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updated_at;
    
    public function __toString() {
        return $this->title;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
    * @ORM\PrePersist
    */
   public function setCreatedValue()
   {
       $this->created_at = new \DateTime();
   }

   /**
    * @ORM\PreUpdate
    */
   public function setUpdatedValue()
   {
       $this->updated_at = new \DateTime();
   }

   

    /**
     * Set title
     *
     * @param string $title
     * @return Album
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return Album
     */
    public function setYear($year)
    {
        $this->year = $year;
    
        return $this;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Album
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Album
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    
        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set band
     *
     * @param \Mediatheque\MusiqueBundle\Entity\Band $band
     * @return Album
     */
    public function setBand(\Mediatheque\MusiqueBundle\Entity\Band $band = null)
    {
        $this->band = $band;
    
        return $this;
    }

    /**
     * Get band
     *
     * @return \Mediatheque\MusiqueBundle\Entity\Band 
     */
    public function getBand()
    {
        return $this->band;
    }

    /**
     * Set artist
     *
     * @param \Mediatheque\MusiqueBundle\Entity\Artist $artist
     * @return Album
     */
    public function setArtist(\Mediatheque\MusiqueBundle\Entity\Artist $artist = null)
    {
        $this->artist = $artist;
    
        return $this;
    }

    /**
     * Get artist
     *
     * @return \Mediatheque\MusiqueBundle\Entity\Artist 
     */
    public function getArtist()
    {
        return $this->artist;
    }

    /**
     * Set genre
     *
     * @param \Mediatheque\MusiqueBundle\Entity\Genre $genre
     * @return Album
     */
    public function setGenre(\Mediatheque\MusiqueBundle\Entity\Genre $genre = null)
    {
        $this->genre = $genre;
    
        return $this;
    }

    /**
     * Get genre
     *
     * @return \Mediatheque\MusiqueBundle\Entity\Genre 
     */
    public function getGenre()
    {
        return $this->genre;
    }
}